package pl.pragmatists.warsjawa.dao;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class ExtensionDaoTest extends DaoTest {

    private ExtensionDao extensionDao;
    private int uniqueId = 1;

    @Before
    public void setUp() {
        extensionDao = new ExtensionDao(entityManager);
    }

    @Test
    public void shouldPersistAPhone() {
        Extension aExtension = anExtension();

        transaction().begin();
        extensionDao.save(aExtension);
        transaction().commit();


        transaction().begin();
        assertEquals(extensionDao.all(), Arrays.asList(aExtension));
        transaction().rollback();
    }

    @Test
    public void shouldCascadePersistFunctionKey() throws Exception {
        //given
        Extension aExtension = persisted(anExtension());
        FunctionKey aFunctionKey = aFunctionKey();
        entityManager.clear();

        //when
        aExtension.setFunctionKeys(list(aFunctionKey));
        transaction().begin();
        extensionDao.save(aExtension);
        transaction().commit();
        entityManager.clear();

        //then
        transaction().begin();
        assertEquals(theOnlyFunctionKeyInDB(), aFunctionKey);
        transaction().rollback();
    }

    @Test
    public void shouldPersistInboundNumber() throws Exception {
        //given
        Extension aExtension = persisted(anExtension());
        InboundNumber inboundNumber = persisted(new InboundNumber("+48123456789"));
        entityManager.clear();

        //when
        aExtension.setInboundNumbers(asList(inboundNumber));
        transaction().begin();
        extensionDao.save(aExtension);
        transaction().commit();
        entityManager.clear();

        //then
        transaction().begin();
        assertEquals(theOnlyInboundNumberInDB(), inboundNumber);
        transaction().rollback();
    }

    @Test
    public void shouldPersistNewInboundNumbers() throws Exception {
        //given
        Extension aExtension = persisted(anExtension());
        InboundNumber inboundNumber = persisted(new InboundNumber("+48123456789"));
        entityManager.clear();

        //when

        aExtension.setInboundNumbers(asList(inboundNumber));
        transaction().begin();
        extensionDao.save(aExtension);
        transaction().commit();
        entityManager.clear();

        //then
        transaction().begin();
        assertEquals(theOnlyInboundNumberInDB(), inboundNumber);
        transaction().rollback();
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotCascadePersistInboundNumber() throws Exception {
        //given
        Extension aExtension = persisted(anExtension());
        InboundNumber inboundNumber = new InboundNumber("+48123456789");
        entityManager.clear();

        //when
        aExtension.setInboundNumbers(asList(inboundNumber));
        transaction().begin();
        extensionDao.save(aExtension);
        transaction().commit();
        entityManager.clear();
    }

    @Test
    public void shouldSaveDependentEntityEvenWithoutCascade() throws Exception {
        //given
        Extension anExtension = persisted(anExtension());
        Device aDevice = aDevice();
        entityManager.clear();

        transaction().begin();
        aDevice = entityManager.merge(aDevice);
        aDevice.setExtension(anExtension);
        readFromDB(anExtension).getDevices().add(aDevice);

        transaction().commit();
        entityManager.clear();

        //when
        transaction().begin();
        anExtension = readFromDB(anExtension);
        anExtension.getDevices().iterator().next().setName("at home");
        anExtension.setName("Fiona");
        transaction().commit();
        entityManager.clear();

        //then
        assertEquals("at home", readFromDB(aDevice).getName());
        assertEquals("Fiona", readFromDB(anExtension).getName());
    }

    @Test
    public void shouldNotMergeDependentEntityWithoutCascade() throws Exception {
        //given
        Extension anExtension = persisted(anExtension());
        Device aDevice = aDevice();

        transaction().begin();
        entityManager.merge(aDevice);
        aDevice.setExtension(anExtension);
        anExtension.getDevices().add(aDevice);

        transaction().commit();
        entityManager.clear();

        //when
        transaction().begin();
        anExtension.getDevices().iterator().next().setName("at home");
        entityManager.merge(anExtension);
        transaction().commit();
        entityManager.clear();


        //then
        assertNull(readFromDB(aDevice).getName());
    }

    private Device readFromDB(Device aDevice) {
        return entityManager.find(Device.class, aDevice.id());
    }

    private Extension readFromDB(Extension anExtension) {
        return entityManager.find(Extension.class, anExtension.id());
    }

    private Device aDevice() {
        return new Device();
    }

    private InboundNumber theOnlyInboundNumberInDB() {
        return extensionDao.all().get(0).getInboundNumbers().get(0);
    }

    private ArrayList<FunctionKey> list(FunctionKey aFunctionKey) {
        return new ArrayList<FunctionKey>(asList(aFunctionKey));
    }

    private FunctionKey theOnlyFunctionKeyInDB() {
        return extensionDao.all().get(0).getFunctionKeys().get(0);
    }

    private FunctionKey aFunctionKey() {
        return new FunctionKey(new FunctionKeyId(String.valueOf(uniqueId++)), 1);
    }

    private <T> T persisted(T aPhone) {
        transaction().begin();
        T phone = entityManager.merge(aPhone);
        transaction().commit();
        return phone;
    }

    private Extension anExtension() {
        return new Extension(extensionDao.nextId(), "Maciej");
    }

}