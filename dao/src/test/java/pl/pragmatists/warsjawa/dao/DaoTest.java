package pl.pragmatists.warsjawa.dao;

import org.junit.After;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DaoTest {
    private EntityManagerFactory entityManagerFactory;
    protected EntityManager entityManager;


    public DaoTest() {
        this("database");
    }

    public DaoTest(String sourceName) {
        entityManagerFactory = Persistence.createEntityManagerFactory(sourceName);
        entityManager = entityManagerFactory.createEntityManager();

    }

    @After
    public void tearDown() throws Exception {
        entityManager.close();
        entityManagerFactory.close();
    }

    protected EntityTransaction transaction() {
        return entityManager.getTransaction();
    }

}
