package pl.pragmatists.warsjawa.dao;

import javax.persistence.*;
import java.util.UUID;

@Table
@Entity
public class Device {

    @Id
    @Embedded
    private ExtensionId id;

    @ManyToOne(targetEntity = Extension.class)
    private Extension extension;

    @Column
    private String name;

    public Device() {
        this.id = new ExtensionId(UUID.randomUUID().toString());
    }


    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ExtensionId id() {
        return id;
    }
}
