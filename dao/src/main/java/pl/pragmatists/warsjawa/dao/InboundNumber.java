package pl.pragmatists.warsjawa.dao;

import javax.persistence.*;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Table
public class InboundNumber {

    @Id
    private long id;

    @Column(unique = true)
    private String number;

    @ManyToOne(targetEntity = Extension.class)
    private Extension extension;

    private static AtomicInteger uniqueId = new AtomicInteger(1);

    protected InboundNumber() {
        //Hibernate
    }

    public InboundNumber(String number) {
        this.id = uniqueId.getAndIncrement();
        this.number = number;
    }

    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(id).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InboundNumber other = (InboundNumber) obj;
        return (id == other.id);
    }


}
