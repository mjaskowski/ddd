package pl.pragmatists.warsjawa.dao;

import javax.persistence.EntityManager;
import java.rmi.server.UID;
import java.util.List;

public class ExtensionDao {

    private EntityManager entityManager;

    public ExtensionDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Extension save(Extension extension) {
        return entityManager.merge(extension);
    }

    public ExtensionId nextId() {
        return new ExtensionId(new UID().toString());
    }

    public List<Extension> all() {
        return entityManager.createQuery("from Extension", Extension.class).getResultList();
    }

}
