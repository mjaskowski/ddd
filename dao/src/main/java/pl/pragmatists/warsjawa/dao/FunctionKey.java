package pl.pragmatists.warsjawa.dao;

import javax.persistence.*;

@Entity
@Table
public class FunctionKey {

    @EmbeddedId
    private FunctionKeyId id;

    private FunctionKey.Number number;

    @ManyToOne
    private Extension target;

    protected FunctionKey() {
        //Hibernate
    }

    public FunctionKey(FunctionKeyId id, int number) {
        super();
        this.id = id;
        this.number = new FunctionKey.Number(number);
    }

    public Extension getTarget() {
        return target;
    }

    public void setTarget(Extension target) {
        this.target = target;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FunctionKey other = (FunctionKey) obj;
        return id.equals(other.id);
    }

    @Embeddable
    public static class Number {

        private int number;

        public Number(int number) {
            this.number = number;
        }

        protected Number() {
            //Hibernate
        }
    }

}
