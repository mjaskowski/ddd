package pl.pragmatists.warsjawa.inboundNumber.domain;

import com.google.common.base.Function;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import pl.pragmatists.warsjawa.extension.ExtensionId;

import javax.persistence.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;

@Entity
@Table
public class InboundNumber {

    @Id
    private long id;

    @Embedded
    private Number number;

    @Embedded
    @AttributeOverride(column = @Column(name = "extension_id"), name = "id")
    private ExtensionId extensionId;

    private static AtomicInteger uniqueId = new AtomicInteger(1);

    protected InboundNumber() {
        //Hibernate
    }

    public InboundNumber(Number number, ExtensionId extensionId) {
        this.id = uniqueId.getAndIncrement();
        this.number = number;
        this.extensionId = extensionId;
    }


    @Override
    public int hashCode() {
        return Long.valueOf(id).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        List<String> excludeFields = asList("id");
        return EqualsBuilder.reflectionEquals(this, obj, excludeFields);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public Number getNumber() {
        return number;
    }

    public static Function<? super InboundNumber, ? extends Number> extractNumber() {
        return new Function<InboundNumber, Number>() {

            @Override
            public Number apply(InboundNumber inboundNumber) {
                return inboundNumber.getNumber();
            }
        };
    }


}
