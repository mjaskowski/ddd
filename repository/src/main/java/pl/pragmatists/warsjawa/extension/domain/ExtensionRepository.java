package pl.pragmatists.warsjawa.extension.domain;

import pl.pragmatists.warsjawa.extension.ExtensionId;


public interface ExtensionRepository {
    void add(Extension event);

    ExtensionId nextId();

    Iterable<Extension> all();

    void remove(Extension extension);

    Extension find(ExtensionId id);
}
