package pl.pragmatists.warsjawa.inboundNumber.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.pragmatists.warsjawa.customer.CustomerId;
import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.extension.domain.Extension;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepository;
import pl.pragmatists.warsjawa.extension.infrastructure.inmemory.InMemoryExtensionRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static org.fest.assertions.api.Assertions.assertThat;

public abstract class InboundNumberRepositoryTest {

    private ExtensionRepository extensionRepository;
    private CustomerId customerId;
    private InboundNumberRepository inboundNumberRepository;
    private int uniqueId = 1;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    @Before
    public void beginTransaction() {
        extensionRepository = new InMemoryExtensionRepository();
        inboundNumberRepository = inboundNumberRepository();
        customerId = new CustomerId("a customer");
        entityManager.getTransaction().begin();
    }

    protected abstract InboundNumberRepository inboundNumberRepository();
    @After
    public void commitTransaction() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    public void shouldPersistInboundNumbers() {
        // given
        Extension extension = aPhone();
        givenInRepository(extension);
        InboundNumber anInboundNumber = aPhoneInboundNumber(extension);

        // when
        inboundNumberRepository.add(anInboundNumber);

        // then
        flushAndClear();
        assertThat(inboundNumberRepository.all()).containsOnly(anInboundNumber);
    }

    private pl.pragmatists.warsjawa.inboundNumber.domain.Number number(String number) {
        return new Number(number);
    }

    private void givenInRepository(InboundNumber... inboundNumbers) {
        for (InboundNumber inboundNumber : inboundNumbers) {
            inboundNumberRepository.add(inboundNumber);
        }
    }

    private void givenInRepository(Extension... extensions) {
        for (Extension extension : extensions) {
            extensionRepository.add(extension);
        }
    }

    private Extension aPhone() {
        return new Extension(extensionRepository.nextId(), customerId, "Maciej");
    }

    private InboundNumber aPhoneInboundNumber(Extension extension) {
        return aPhoneInboundNumber(number("+481234" + uniqueId++), extension.id());
    }

    private InboundNumber aPhoneInboundNumber(Number number,
                                              ExtensionId id) {
        return new InboundNumber(number, id);
    }

    private void flushAndClear() {
        entityManager.flush();
        entityManager.clear();
    }

}