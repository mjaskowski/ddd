package pl.pragmatists.warsjawa.extension.infrastructure.hibernate;

import org.junit.After;
import org.junit.Before;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepository;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepositoryTest;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateExtensionRepositoryTest extends ExtensionRepositoryTest {

    private final EntityManager entityManager;
    private final EntityManagerFactory entityManagerFactory;

    public HibernateExtensionRepositoryTest() {
        entityManagerFactory = Persistence.createEntityManagerFactory("database");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    protected ExtensionRepository extensionRepository() {
        return new HibernateExtensionRepository(entityManager);
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void commitTransaction() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Override
    protected void flushAndClear() {
        entityManager.flush();
        entityManager.clear();
    }
}