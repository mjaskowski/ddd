package pl.pragmatists.warsjawa.customer;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Customer {

    @EmbeddedId
    private CustomerId id;

    private String name;

    protected Customer() {
        //Hibernate
    }

    public Customer(CustomerId id, String name) {
        this.id = id;
        this.name = name;
    }

}
