package pl.pragmatists.warsjawa.inmemory.extension;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.collect.Lists.newArrayList;
import static org.fest.assertions.api.Assertions.assertThat;

public class ExtensionRepositoryTest {

    private ExtensionRepository extensionRepository;

    @Before
    public void setUp() {
        extensionRepository = new ExtensionRepository();
    }

    @Test
    public void shouldPersistAnExtension() {
        //given
        Extension extension = anExtension();

        //when
        extensionRepository.add(extension);

        //then
        assertThat(extensionRepository.all()).containsOnly(extension);

    }

    private Extension anExtension() {
        return new Extension(extensionRepository.nextId(), "Maciej");
    }

}
